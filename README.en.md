# Mind+ all-in-one air quality sensor (RS485) graphical library

#### Introduction
This graphical library is based on the information reported by the **RS485 all-in-one air quality sensor**, which is parsed and processed to make it directly available for you to drag and drop the parameter data reported by the sensor, such as **Temperature**, **Humidity**, **CO2**, **TVOC**, **CH2O**, **PM10**, **PM2.5**.

 **Testing hardware**:

https://shop221040643.taobao.com

https://item.taobao.com/item.htm?id=678612499027

All-in-one air quality sensor Graphical programming Self-developed Source code provided Primary and secondary schools Creator AI

#### Software Architecture
Developed based on Mind+ V1.7.2 RC3.0

#### Installation tutorial

Method 1. use [**https://gitee.com/LNSFAIoT/mindplus-all-in-one-air-quality-sensor.git**] **copy** in the search bar of mind+ **user library** for online download.

Method 2. Select **Direct Download** [ **lnsfaiot-rj485-air-quality-sensor-thirdex-V0.0.2.mpext** ] file and import it directly **externally** .

#### Instructions for use

Screenshot of building blocks

![输入图片说明](image/RJ485%E7%A9%BA%E6%B0%94%E8%B4%A8%E9%87%8F%E4%BC%A0%E6%84%9F%E5%99%A8-mind+%E7%A7%AF%E6%9C%A8.png)

Screenshot of RJ485 type-all-in-one sensor test code

![输入图片说明](image/RJ485%E5%9E%8B%E5%A4%9A%E5%90%88%E2%80%94%E4%BC%A0%E6%84%9F%E5%99%A8%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81.png)

RJ485 type-Screenshot of the reported Alibaba-Cloud-IoT-Platform-all-in-one sensor test code

![输入图片说明](image/RJ485%E5%9E%8B%E4%B8%8A%E6%8A%A5%E9%98%BF%E9%87%8C%E4%BA%91loT%E5%A4%9A%E5%90%88%E2%80%94%E4%BC%A0%E6%84%9F%E5%99%A8%E6%B5%8B%E8%AF%95%E4%BB%A3%E7%A0%81.png)

#### Participating Contributions

 **Lingji Artificial Intelligence Literacy Education Community** 

 **WeChat: ** **Artificial Intelligence Literacy Education Community** 

![输入图片说明](image/%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E7%B4%A0%E5%85%BB%E6%95%99%E8%82%B2%E5%85%B1%E5%90%8C%E4%BD%93%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)

#### Donate
If you find our open source software helpful, please scan the QR code below to reward us with a cup of coffee.

![输入图片说明](image/%E8%AF%B7%E5%A4%9A%E5%A4%9A%E6%94%AF%E6%8C%81%E6%88%91%E4%BB%AC.jpg)

#### Contact Us
For **Feedback** and **Cooperation Intention** contact, you can scan the code to add the following **Corporate WeChat**.

Ms. Huang.

![输入图片说明](image/%E9%BB%84%E8%80%81%E5%B8%88-%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.jpg)

Mr. Zeng.

![输入图片说明](image/%E9%BB%84%E8%80%81%E5%B8%88-%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.jpg)